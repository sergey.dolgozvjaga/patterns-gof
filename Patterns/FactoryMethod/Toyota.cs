﻿
namespace Patterns.FactoryMethod
{
    public class Toyota : Car
    {
        private string carName;
        private int carPrice;
        private int carReleaseYear;

        public Toyota(string carName, int carPrice, int carReleaseYear)
        {
            this.carName = carName;
            this.carPrice = carPrice;
            this.carReleaseYear = carReleaseYear;
        }


        public override string GetCarName()
        {
            return this.carName;
        }


        public override int GetCarPrice()
        {
            return this.carPrice;
        }


        public override int GetCarReleaseYear()
        {
            return this.carReleaseYear;
        }
    }
}
