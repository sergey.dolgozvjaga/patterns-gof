﻿
namespace Patterns.FactoryMethod
{
    public abstract class Car
    {
        public abstract string GetCarName();
        public abstract int GetCarPrice();
        public abstract int GetCarReleaseYear();

        
        public override string ToString()
        {
            return "CarName = " + this.GetCarName() + ", CarPrice = " + this.GetCarPrice() + ", CarReleaseYear = " + this.GetCarReleaseYear();
        }
    }
}
