﻿using System;

namespace Patterns.FactoryMethod
{
    public class CarFactory
    {
        public static Car getCar(string carType, string carName, int carPrice, int carReleaseYear)
        {

            switch (carType)
            {
                case "Audi": return new Audi(carName, carPrice, carReleaseYear);
                case "Toyota": return new Toyota(carName, carPrice, carReleaseYear);
                case "Maserati": return new Maserati(carName, carPrice, carReleaseYear);
            }
            return null;
        }
    }
}
