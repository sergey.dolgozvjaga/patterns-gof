﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Patterns.TemplateMethod
{
    public class ChessGame : Games
    {
        public override string InitializeGame() { return "Initialize chess"; }

        public override string StartPlay() { return "Let's play chess"; }

        public override string Pause() { return "Let's take a pause for beer"; }

        public override string EndPlay() { return "Finish game chess"; }
    }
}
