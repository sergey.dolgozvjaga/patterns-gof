﻿using System;

namespace Patterns.TemplateMethod
{
    public abstract class Games
    {
        public abstract string InitializeGame();
        public abstract string StartPlay();
        public abstract string Pause();
        public abstract string EndPlay();


        /** Method describes lifecycle of any game */

        public string Play()
        {
            InitializeGame();
            StartPlay();
            Pause();
            EndPlay();

            return "game starts ... and ... game is over";
        }
    }
}
