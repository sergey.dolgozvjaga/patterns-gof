﻿
namespace Patterns.TemplateMethod
{
    public class BadmintonGame : Games
    {
        public override string InitializeGame() { return "Initialize badminton"; }

        public override string StartPlay() { return "Let's play badminton"; }

        public override string Pause() { return "Let's take a pause for coffee"; }

        public override string EndPlay() { return "Finish game badminton"; }
    }
}
