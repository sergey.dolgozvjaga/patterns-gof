﻿
namespace Patterns.TemplateMethod
{
    public class BasketballGame : Games
    {
        public override string InitializeGame() { return "Initialize basketball"; }

        public override string StartPlay() { return "Let's play basketball"; }

        public override string Pause() { return "Let's take a pause for water"; }

        public override string EndPlay() { return "Finish game basketball"; }
    }
}
